﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Home.Framework.Infrastructure
{
    public static partial class StringHelper
    {
        public static uint GetUnsignedHashCode(this string str)
        {
            return (uint)str.GetHashCode();
        }

        /// <summary>
        /// 获取字符串的MD5哈希值
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns>System.String.</returns>
        public static string GetMD5HashCode(this string str, string encoding = "UTF-8")
        {
            return MD5Provider.GetHashCode(str, encoding);
        }

        /// <summary>
        /// 字符串掩码
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="startIndex">掩码起始索引值,从0开始</param>
        /// <param name="length">掩码长度</param>
        /// <param name="mask">掩码标志符</param>
        /// <returns>System.String.</returns>
        public static string Mask(this string str, int startIndex, int length, char mask = '*')
        {
            char[] array = str.ToCharArray();
            if (startIndex >=0 && startIndex < array.Length)
            {
                for (int index = startIndex, count = 0; index < array.Length && count < length; ++count)
                {
                    array[index++] = mask;
                }
                return string.Join(string.Empty, array);
            }
            else
            {
                throw new IndexOutOfRangeException("索引值异常，请确认字符串中是否存在该索引值！");
            }
        }

        public static string ConvertEncoding(this string str, string srcEncoding = "GB2312", string destEncoding = "UTF-8")
        {
            // 源格式编码
            Encoding src = Encoding.GetEncoding(srcEncoding);
            // 源格式编码
            byte[] buffer = src.GetBytes(str);

            // 目标格式编码
            Encoding dest = Encoding.GetEncoding(destEncoding);
            // 转换为 目标格式编码
            byte[] buffer2 = Encoding.Convert(src, dest, buffer, 0, buffer.Length);

            return src.GetString(buffer2, 0, buffer2.Length);
        }

        /// <summary>
        /// 将字符串转化为URL编码方式
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>System.String.</returns>
        public static string GetUrlEncode(this string str)
        {
            return HttpUtility.UrlEncode(str);
        }

        /// <summary>
        /// 对字符串进行URL解码
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>System.String.</returns>
        public static string GetUrlDecode(this string str)
        {
            return HttpUtility.UrlDecode(str);
        }

        /// <summary>
        /// 解密后的明文字符串
        /// </summary>
        /// <param name="origin">解密后的明文</param>
        /// <returns>参数字典</returns>
        public static IDictionary<string, string> GetParamDictionary(this string origin, char masterSeparator = '&', char detailSeparator = '=')
        {
            // 原始方法
            //string[] paramList = origin.Split('&');
            //IDictionary<string, string> param = new Dictionary<string, string>();
            //foreach (var item in paramList)
            //{
            //    string[] itemArray = item.Split('=');
            //    param.Add(itemArray[0], itemArray[1]);
            //}
            //return param;
            return origin.Split(masterSeparator).ToDictionary(m => m.Split(detailSeparator).FirstOrDefault(), n => n.Split(detailSeparator).LastOrDefault());
        }
    }
}
