﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    public static partial class CommonUtils
    {
        public static readonly DateTime defaultTime = DateTime.Parse("1900/01/01");

        /// <summary>
        /// 填充Model对象默认值
        /// </summary>
        /// <typeparam name="T">Model类型</typeparam>
        /// <param name="target">要填充默认值的对象</param>
        /// <returns>填充默认值之后的对象</returns>
        public static T Pad<T>(T target) where T : class
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                Type type = property.PropertyType;
                if (type.Equals(typeof(DateTime)))
                {
                    property.SetValue(target, defaultTime);
                }
                else if (type.Equals(typeof(string)))
                {
                    property.SetValue(target, string.Empty);
                }
            }
            return target;
        }

        public static void Swap(ref int a, ref int b)
        {
            a ^= b;
            b ^= a;
            a ^= b;
        }
    }
}
