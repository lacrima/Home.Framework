﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    public class ServiceTimer
    {
        public ServiceTimer()
        {
            this.TimeStack = new Stack<DateTime>();
            this.TimeStack.Push(this.CreateTime = DateTime.Now);
            this.TerminalTime = this.CreateTime;
        }

        /// <summary>
        /// 计时器创建时间
        /// </summary>
        /// <value>The create time.</value>
        public DateTime CreateTime { get; private set; }

        /// <summary>
        /// 计时器时间存放栈
        /// </summary>
        /// <value>The time stack.</value>
        public Stack<DateTime> TimeStack { get; private set; }

        /// <summary>
        /// 计时器停止计时时间
        /// </summary>
        /// <value>The terminal time.</value>
        public DateTime TerminalTime { get; private set; }

        public TimeSpan Interval 
        {
            get
            {
                return DateTimeUtils.GetInterval(this.CreateTime, this.TerminalTime);
            }
        }

        public double GetTimeStamp()
        {
            return DateTimeUtils.ParseTimeStamp(this.TerminalTime);
        }

        public void UpdateTimer()
        {
            this.TimeStack.Push(this.TerminalTime = DateTime.Now);
        }

        public void UpdateTimer(DateTime target)
        {
            this.TimeStack.Push(this.TerminalTime = target);
        }
    }
}
