﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    /// <summary>
    /// 容器服务模型
    /// </summary>
    /// <typeparam name="TMaster">主结构信息类</typeparam>
    /// <typeparam name="TDetail">从属结构信息类</typeparam>
    public class Container<TMaster,TDetail>      
        where TMaster : class, new()
        where TDetail : class, new()
    {

        public Container()
        {
            this.Master = new TMaster();
            this.Detail = new List<TDetail>();
        }

        /// <summary>
        /// 主结构信息
        /// </summary>
        /// <value>The base information.</value>
        public TMaster Master { get; set; }

        /// <summary>
        /// 从属结构信息列表
        /// </summary>
        public List<TDetail> Detail { get; set; }
    }

}
