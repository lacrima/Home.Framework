﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    public static partial class TriangleFormula
    {
        /// <summary>
        /// 求三角形周长
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        /// <returns>System.Double.</returns>
        public static double Perimeter(double a, double b, double c)
        {
            return a + b + c;
        }

        /// <summary>
        /// 计算三角形半周长
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        /// <returns>System.Double.</returns>
        public static double SemiPerimeter(double a, double b, double c)
        {
            return Perimeter(a, b, c) * 0.5d;
        }

        /// <summary>
        /// 使用海伦公式求三角形面积
        /// 公式：S=√[p(p-a)(p-b)(p-c)] （海伦公式）
        /// 参照链接：http://www.zybang.com/question/1445f60f12425825ee76f9191f668196.html
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        /// <returns>System.Double.</returns>
        public static double HeronFormulaSquare(double a, double b, double c)
        {
            // 计算半周长-Semi-perimeter
            double p = SemiPerimeter(a, b, c);
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }

        /// <summary>
        /// 计算内切圆半径
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        /// <returns>System.Double.</returns>
        public static double IncircleRadius(double a, double b, double c)
        {
            return HeronFormulaSquare(a, b, c) / SemiPerimeter(a, b, c);
        }

        /// <summary>
        /// 计算外接圆半径
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        /// <returns>System.Double.</returns>
        public static double CircumcircleRadius(double a, double b, double c)
        {
            return (a * b * c) / (4 * HeronFormulaSquare(a, b, c));
        }
    }
}
